﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chart.Mvc.ComplexChart;

namespace CiclumTestTask.Models
{
    public class PricesChartData : IComplexPricesChartData
    {
        public PricesChartData(IProduct singleProduct)
        {
            Labels = singleProduct.Prices.OrderBy(o => o.Date).Select(priceToDate => priceToDate.Date.Date.ToString("dd/MM")).ToList();
            Dataset = new List<ComplexDataset> {
                new ComplexDataset{
                     Data = singleProduct.Prices.OrderBy(o => o.Date).Select(priceToDate => Convert.ToDouble(priceToDate.Price)).ToList(),
                     Label = Convert.ToString(singleProduct.ProductId),
                     FillColor = "rgba(220,220,220,0.2)",
                     StrokeColor = "rgba(220,220,220,1)",
                     PointColor = "rgba(220,220,220,1)",
                     PointStrokeColor = "#fff",
                     PointHighlightFill = "#fff",
                     PointHighlightStroke = "rgba(220,220,220,1)",
                }
            }; 
        }
        public IEnumerable<string> Labels{ get; set; }

        public IEnumerable<ComplexDataset> Dataset { get;set; }
    }
}