﻿using CiclumTestTask.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParserToDb
{
    public class WebManager
    {
        public static async Task WriteToDbProducts()
        {
            var urlsToSave = await UrlsFromParfums();
            var resultList = await Task.WhenAll(urlsToSave.Select(i => ProductFromParfums(i)));
            using (var context = new ProductContext())
            {
                context.Products.AddRange(resultList);
                context.SaveChanges();
            }
        }
        public static async Task<List<string>> UrlsFromParfums()
        {
            List<string> links = new List<string>();

            string url = "https://parfums.ua/category/cosmetics-for-the-face/none/1/60";
            var web1 = new HtmlWeb();
            var doc1 = await web1.LoadFromWebAsync(url);
            var nodes = doc1.DocumentNode.SelectNodes("//div[@class='product product--big js-product ']");
            foreach (var node in nodes)
            {
                links.Add(node.Attributes["data-url"].Value);
            }
            return links;
        }

        public static async Task<SingleProduct> ProductFromParfums(string url)
        {
            var web1 = new HtmlWeb();
            var doc1 = await web1.LoadFromWebAsync(url);
            var title = doc1.DocumentNode.SelectSingleNode("//h2[@class='productpage__desctitle']").InnerText;
            var description = doc1.DocumentNode.SelectSingleNode("//li[@class='productpage__itemtab js-tab-content js-accordion-content']//p").InnerText;
            var imageUrl = @"https://parfums.ua" + doc1.DocumentNode.SelectSingleNode("//img[@id='product-image']").Attributes["data-src"].Value;
            SingleProduct product = new SingleProduct() { Name = title, ImageUrl = imageUrl, Description = description, ProductUrl = url };
            return product;
        }

        public static async Task WriteToDbPrices()
        {
            using (var context = new ProductContext())
            {
                var productUrls = context.Products.ToList();
                var resultList = await Task.WhenAll(productUrls.Select(i => PricesFromParfums(i)));
                context.Prices.AddRange(resultList);
                context.SaveChanges();
            }
        }
        public static async Task<ProductPriceToDate> PricesFromParfums(SingleProduct product)
        {
            var web1 = new HtmlWeb();
            var doc1 = await web1.LoadFromWebAsync(product.ProductUrl);
            var price = doc1.DocumentNode.SelectSingleNode("//div[@class='product__price--default']//span[@itemprop='price']") != null ? doc1.DocumentNode.SelectSingleNode("//div[@class='product__price--default']//span[@itemprop='price']").InnerText : doc1.DocumentNode.SelectSingleNode("//div[@class='product__price product__price--new']")?.InnerText.Split(' ')[0];
            ProductPriceToDate priceToDate = new ProductPriceToDate() { ProductId = product.ProductId, Price = price , Date = DateTime.UtcNow.Date, Product = product };
            return priceToDate;
        }

    }
}
