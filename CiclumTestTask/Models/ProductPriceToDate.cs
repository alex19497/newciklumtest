﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Web;

namespace CiclumTestTask.Models
{
    [Table("pricesTable")]
    public class ProductPriceToDate
    {
        [Required]
        [Key]
        public int ProductPriceToDateId { get; set; }

        public string Price { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
       
        public virtual SingleProduct Product { get; set; }
    }
}