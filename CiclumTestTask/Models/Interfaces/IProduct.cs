﻿using System.Collections.Generic;

namespace CiclumTestTask.Models
{
    public interface IProduct
    {
        string Description { get; set; }
        string ImageUrl { get; set; }
        string Name { get; set; }
        List<ProductPriceToDate> Prices { get; set; }
        int ProductId { get; set; }
        string ProductUrl { get; set; }
        IComplexPricesChartData PricesChart { get; set; }
    }
}