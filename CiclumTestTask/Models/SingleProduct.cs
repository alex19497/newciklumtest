﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Chart.Mvc.ComplexChart;

namespace CiclumTestTask.Models
{
    [Table("productTable")]
    public class SingleProduct : IProduct
    {
        [Key]
        public int ProductId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string ProductUrl { get; set; }

        public virtual List<ProductPriceToDate> Prices { get; set; }
        [NotMapped]
        public IComplexPricesChartData PricesChart { get; set; }

        
    }
    public class ProductContext : DbContext
    {
        public DbSet<SingleProduct> Products { get; set; }

        public DbSet<ProductPriceToDate> Prices { get; set; }
    }
}