﻿using System;
using System.Threading.Tasks;
using CiclumTestTask.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParserToDb;

namespace ParserToDbTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task ProductFromParfumsTestAsync()
        {
            var result = await WebManager.ProductFromParfums(@"https://parfums.ua/product/uvlazhnyayushij-krem-dlya-normalnoj-i-kombinirovannoj-kozhi-mixa-moisturizing-balancing-cream");
            Assert.AreEqual("Увлажняющий крем для нормальной и комбинированной кожи Mixa Moisturizing Balancing Cream", result.Name);
        }

        [TestMethod]
        public async Task PricesFromParfumsTestAsync()
        {
            SingleProduct singleProduct = new SingleProduct() { Name = "TestName", ImageUrl = "imageUrl", Description = "description", ProductUrl = @"https://parfums.ua/product/uvlazhnyayushij-krem-s-placentoj-dlya-zhirnoj-kozhi-christina-elastin-collagen-moisture-cream" };
            var result = await WebManager.PricesFromParfums(singleProduct);
            Assert.AreEqual("450", result.Price);
        }
    }
}
