﻿using System.Collections.Generic;
using Chart.Mvc.ComplexChart;

namespace CiclumTestTask.Models
{
    public interface IComplexPricesChartData
    {
        IEnumerable<ComplexDataset> Dataset { get; set; }
        IEnumerable<string> Labels { get; set; }
    }
}