﻿using System;
using System.Collections.Generic;
using AntiPShared;
using HtmlAgilityPack;
using System.Web;
using System.Linq;
using ReadSharp;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Schedulers;

namespace ConsoleApp1
{
    class Program
    {
        public static List<string> UrlsFromParfums()
        {
            List<string> links = new List<string>();

            string url = "https://parfums.ua/category/cosmetics-for-the-face/none/1/60";
            var web1 = new HtmlWeb();
            // var doc1 = web1.LoadFromWebAsync(url);
            var doc1 = web1.LoadFromBrowser(url, html =>
            {
                return !html.Contains("<div class=\"products__wrapper\"></div>");
            });
            // var nodes0 = doc1.DocumentNode.SelectNodes("");
            var nodes = doc1.DocumentNode.SelectNodes("div").Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value == "product product--big js - product");
            foreach (var node in nodes)
            {
                links.Add(node.Attributes["data-url"].Value);
            }
            //var nodes = doc1.DocumentNode.SelectNodes("//a").Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value == "gs-title");
            //foreach (var node in nodes)
            //{
            //    links.Add(node.Attributes["href"].Value);
            //    //links.Add(node.FirstChild.Attributes["href"].Value);
            //}

            return links;
        }
        static void Main(string[] args)
        {
            
        }
    }
}
