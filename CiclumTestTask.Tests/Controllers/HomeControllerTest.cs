﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CiclumTestTask;
using CiclumTestTask.Controllers;
using CiclumTestTask.Models;

namespace CiclumTestTask.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            HomeController controller = new HomeController();
           
            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Details()
        {
            HomeController controller = new HomeController();

            var result = controller.Details(2) as ViewResult;

            var product = (SingleProduct)result.ViewData.Model;

            Assert.AreEqual("Гидрогелевые патчи для глаз с золотым комплексом +5 Petitfee Gold Hydrogel Eye Patch", product.Name);
        }
    }
}
