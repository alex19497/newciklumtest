﻿using CiclumTestTask.Models;
using CiclumTestTask.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CiclumTestTask.Controllers
{
    public class HomeController : Controller
    { 
        private ProductContext db = new ProductContext();

        public ActionResult Index()
        {
            //Stopwatch watch = new Stopwatch();
            //watch.Start();
            //await WebManager.WriteToDbProducts();
            //watch.Stop();
            //ViewBag.WatchMilliseconds = watch.ElapsedMilliseconds;


            //Stopwatch watch = new Stopwatch();
            //watch.Start();
            //await WebManager.WriteToDbPrices();
            //watch.Stop();
            //ViewBag.WatchMilliseconds = watch.ElapsedMilliseconds;


           return View(db.Products.ToList());
        }
       
        public ActionResult Products()
        {
            return View(db.Products.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IProduct singleProduct = db.Products.Find(id);
            if (singleProduct == null)
            {
                return HttpNotFound();
            }
            singleProduct.PricesChart = new PricesChartData(singleProduct);
            return View(singleProduct);
        }
    }
}